import requests
import os
import time

def check_ping(ip):
    response = os.system("ping -c 1 -W 1 " + ip)
    if response == 0:
        pingstatus = True
    else:
        pingstatus = False
    return pingstatus

while True:
    if check_ping('gitlab.com'):
        response = os.system("git pull")
        print(response)
    else:
        print('Sin internet')
    time.sleep(10)